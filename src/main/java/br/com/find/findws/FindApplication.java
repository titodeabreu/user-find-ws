package br.com.find.findws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringBootApplication
@EnableReactiveMongoRepositories
public class FindApplication {

	public static void main(String[] args) {
		SpringApplication.run(FindApplication.class, args);
	}

}