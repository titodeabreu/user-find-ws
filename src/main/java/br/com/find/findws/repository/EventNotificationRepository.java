package br.com.find.findws.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import br.com.find.findws.pojo.EventNotification;

@Repository
interface EventNotificationRepository extends ReactiveMongoRepository<EventNotification, String> {
}
