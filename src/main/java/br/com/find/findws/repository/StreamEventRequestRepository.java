package br.com.find.findws.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import br.com.find.findws.pojo.StreamEventRequest;
import reactor.core.publisher.Flux;

@Repository
public interface StreamEventRequestRepository extends ReactiveMongoRepository<StreamEventRequest, String> {

	Flux<StreamEventRequest> findByEventOrderByEventPublishedTimeDesc(String event);
}