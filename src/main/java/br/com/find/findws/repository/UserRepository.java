package br.com.find.findws.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import br.com.find.findws.pojo.User;

@Repository
public interface UserRepository extends ReactiveMongoRepository<User, String> {
}
