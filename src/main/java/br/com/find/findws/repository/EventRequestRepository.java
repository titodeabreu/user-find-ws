package br.com.find.findws.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import br.com.find.findws.pojo.EventRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface EventRequestRepository extends ReactiveMongoRepository<EventRequest, String> {
	
	Flux<EventRequest> findAllByStatus(String status);

	Mono<EventRequest> findById(String id);
	
}