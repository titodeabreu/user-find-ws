package br.com.find.findws.component;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
@EnableWebSocket
public class WebSocketConfiguration implements WebSocketMessageBrokerConfigurer{

	 @Override
	    public void configureMessageBroker(MessageBrokerRegistry config) {
		 config.setApplicationDestinationPrefixes("/app")
         .enableSimpleBroker("/customerRequest","/driverStreamingRequest");
	    }

	 @Override
	 public void registerStompEndpoints(StompEndpointRegistry registry) {
	         registry.addEndpoint("/customers","/drivers")
	                 .setAllowedOrigins("*")
	                 .withSockJS();
	     }
	
}
