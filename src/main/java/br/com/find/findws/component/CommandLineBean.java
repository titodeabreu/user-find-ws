package br.com.find.findws.component;

import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import br.com.find.findws.repository.EventRequestRepository;
import br.com.find.findws.service.CustomerService;
import reactor.core.publisher.Mono;

@Component
public class CommandLineBean {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private EventRequestRepository eventRequestRepository;

	@Bean
	public CommandLineRunner run() {
		return args -> {
			System.out.println("Started!");

			eventRequestRepository.deleteAll().thenMany(Mono.fromRunnable(() -> multipleCallsForEvent())).subscribe();

			//eventRequestRepository.deleteAll().subscribe();

			System.out.println("End!");
		};
	}

	private void multipleCallsForEvent() {

//		for(int i = 0; i < 5; i++) {
//			customerService.registerRequest(null);
//		}

		try {
			Thread.sleep(20000l);
			Timer timer = new Timer();
			timer.scheduleAtFixedRate(new TimerTask() {
				public void run() {
					customerService.registerRequest(null);
				}
			}, 0, 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
