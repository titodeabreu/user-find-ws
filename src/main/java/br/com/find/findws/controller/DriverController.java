package br.com.find.findws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.find.findws.pojo.EventRequest;
import br.com.find.findws.service.DriverService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/driver")
class DriverController {
	
	@Autowired
	private DriverService driverService;

	@GetMapping(value = "/requests", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
	public Flux<EventRequest> requests() {
		return driverService.streamingCreatedEventRequest();
	}
	
	@PostMapping(value = "/acceptEventRequest")
	public Mono<EventRequest> acceptEventRequest() {
		return driverService.acceptEventRequest("1");
	}
}	