package br.com.find.findws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RestController;

import br.com.find.findws.pojo.EventRequest;
import br.com.find.findws.service.CustomerService;

@RestController
public class DriverSocketController {
	
	private SimpMessagingTemplate template;
	
	@Autowired
	private CustomerService customerService;
	
	public DriverSocketController(SimpMessagingTemplate template,CustomerService customerService) {
		this.template = template;
		this.customerService = customerService;
	}
	
	@MessageMapping("/send/message")
	public void onReceivedMessage(String message) {
		EventRequest eventRequest = new EventRequest();
		eventRequest.setCustomer(message);
		customerService.registerRequest(eventRequest);
		this.template.convertAndSend("/driverStreamingRequest", "backend responde essa msg:"+message);
	}

}
