package br.com.find.findws.interfaces;

import java.time.LocalDateTime;

import br.com.find.findws.pojo.EventRequest;
import br.com.find.findws.pojo.Place;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IDriverService {
	public Flux<EventRequest> streamingCreatedEventRequest();

	public Mono<EventRequest> acceptEventRequest(String event);

	public void pushStreamEventRequest(String event, LocalDateTime publishingTime, Place actualPlace);

	public Mono<EventRequest> retrieveActualEvent(String idDriver);
}