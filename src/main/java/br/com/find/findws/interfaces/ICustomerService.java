package br.com.find.findws.interfaces;

import br.com.find.findws.pojo.EventNotification;
import br.com.find.findws.pojo.EventRequest;
import br.com.find.findws.pojo.StreamEventRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ICustomerService {
	
	public String registerRequest(EventRequest eventRequest);

	public Flux<StreamEventRequest> streamingEventFromCustomerRequest(String event);

	public Flux<EventNotification> streaminNotificationForCustomer();

	public Mono<EventRequest> retrieveActualEvent(String idCustomer);
}
