package br.com.find.findws.pojo;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document
public class StreamEventRequest {

	@Id
	private String id;
	private String event;
	private LocalDateTime eventPublishedTime;
	private Place driverEventPlace;
	private boolean read;
}