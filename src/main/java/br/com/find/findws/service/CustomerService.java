package br.com.find.findws.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.find.findws.interfaces.ICustomerService;
import br.com.find.findws.pojo.EventNotification;
import br.com.find.findws.pojo.EventRequest;
import br.com.find.findws.pojo.Place;
import br.com.find.findws.pojo.StreamEventRequest;
import br.com.find.findws.pojo.User;
import br.com.find.findws.repository.EventRequestRepository;
import br.com.find.findws.repository.StreamEventRequestRepository;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Service
public class CustomerService implements ICustomerService {

	private static Logger logger = LoggerFactory.getLogger(CustomerService.class);

	private static final String CUSTOMER_TYPE = "customer";
	private EventRequestRepository eventRequestRepository;
	private StreamEventRequestRepository streamEventRequestRepository;

	public CustomerService(EventRequestRepository eventRequestRespository,
			StreamEventRequestRepository streamEventRequestRepository) {
		this.eventRequestRepository = eventRequestRespository;
		this.streamEventRequestRepository = streamEventRequestRepository;
	}
	
	public List<EventRequest> listAll(){
		return eventRequestRepository.findAll().toStream().collect(Collectors.toList());
	}

	@Override
	public String registerRequest(EventRequest eventRequest) {
		eventRequest = new EventRequest();
		eventRequest.setId(UUID.randomUUID().toString());
		// TODO place from google && googleUtilComponent
		Place location = new Place("Taguatinga", String.valueOf(new Random().nextInt()),
				String.valueOf(new Random().nextInt()));
		Place destination = new Place("Arniqueiras", String.valueOf(new Random().nextInt()),
			String.valueOf(new Random().nextInt()));
		
		eventRequest.setLocation(location);
		eventRequest.setDestination(destination);
		eventRequest.setStatus("new");
		eventRequest.setRequestDate(LocalDateTime.now());
		// FIM CONTROLLER
		logger.info("Register Start for eventRequest:" + eventRequest.toString());
		eventRequestRepository.save(eventRequest).subscribe();	
		logger.info("Register End for eventRequest:" + eventRequest.toString());
		return eventRequest.getId();
	}

	// TODO delete it
	private String newRandomNames() {
		String[] names = "Tito,Rebeka,Alice Maria".split(",");
		return names[new Random().nextInt(names.length)];
	}

	@Override
	public Flux<StreamEventRequest> streamingEventFromCustomerRequest(String event) {
		Flux<Long> interval = Flux.interval(Duration.ofSeconds(1));
		Flux<StreamEventRequest> events = streamEventRequestRepository.findByEventOrderByEventPublishedTimeDesc(event);
		return Flux.zip(interval, events).map(Tuple2::getT2);
	}

	@Override
	public Flux<EventNotification> streaminNotificationForCustomer() {
		return null;
	}

	@Override
	public Mono<EventRequest> retrieveActualEvent(String idCustomer) {
		return null;
	}
}