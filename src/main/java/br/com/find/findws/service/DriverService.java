package br.com.find.findws.service;

import java.time.Duration;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.find.findws.interfaces.IDriverService;
import br.com.find.findws.pojo.EventRequest;
import br.com.find.findws.pojo.Place;
import br.com.find.findws.repository.EventRequestRepository;
import br.com.find.findws.repository.StreamEventRequestRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Service
public class DriverService implements IDriverService {

	private static Logger logger = LoggerFactory.getLogger(DriverService.class);

	private static final String DRIVER_TYPE = "driver";
	private EventRequestRepository eventRequestRepository;
	private StreamEventRequestRepository streamEventRequestRepository;

	public DriverService(EventRequestRepository eventRequestRespository,
			StreamEventRequestRepository streamEventRequestRepository) {
		this.eventRequestRepository = eventRequestRespository;
		this.streamEventRequestRepository = streamEventRequestRepository;
	}

	@Override
	public Flux<EventRequest> streamingCreatedEventRequest() {
		logger.info("Doing Streaming of EventRequest By Status New");
		Flux<Long> interval = Flux.interval(Duration.ofSeconds(1));
		Flux<EventRequest> events = eventRequestRepository.findAllByStatus("new");
		return Flux.zip(interval, events).map(Tuple2::getT2);
	}

	@Override
	public Mono<EventRequest> acceptEventRequest(String event) {
		logger.info("Doing acceptEventRequest by event id: " + event);
		Mono<EventRequest> eventRequest = eventRequestRepository.findById(event);
		eventRequest.subscribe(eventR -> {
			eventR.setDriver("Horror Driver");
			eventR.setAcceptDate(LocalDateTime.now());
			eventR.setStatus("progress");
			logger.info("Event updated: " + eventR.toString());
			eventRequestRepository.save(eventR).subscribe();
		});
		return eventRequest;
	}

	@Override
	public void pushStreamEventRequest(String event, LocalDateTime publishingTime, Place actualPlace) {

	}

	@Override
	public Mono<EventRequest> retrieveActualEvent(String idDriver) {
		return null;
	}
}